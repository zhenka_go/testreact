'use strict'

import React from 'react'
import Btn from './btn'
export default class Table extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            rows: this.addArray(4, 4, {text: ''}),
            top: null,
            left: null,
            currentRow: null,
            currentCol: null,
            classBtn: '',
            btnDel: 'block'
        }
    }


    addArray(rows, cols, content) {
        return Array.from({length: rows}, (row) => Array.from({length: cols}, (col) => content))
    }

    addColRow(cond) {
        let rows = this.state.rows
        if (cond == true) {
            rows.map(function (item) {
                item.push({text: ''})
            })
        } else if (cond == false) {
            let tempArray = []
            for (var i = 0; i < rows[rows.length - 1].length; i++) {
                tempArray.push({text: ''})
            }
            rows.push(tempArray)

        }
        this.setState({
            rows: rows
        })
    }


    delRowCol(cond) {
        let {currentRow, rows} = this.state
        if (cond === true) {
            rows.splice(currentRow, 1)
        } else if (cond === false) {
            let {currentCol, rows} = this.state
            rows.map(function (item) {
                item.splice(currentCol, 1)
            })
        }
        this.setState({
            rows: rows
        })
    }

    hoverHandle(e) {
        this.setState({
            top: e.target.getBoundingClientRect().top,
            left: e.target.getBoundingClientRect().left,
            currentRow: e.target.parentNode.parentNode.rowIndex,
            currentCol: e.target.parentNode.cellIndex
        })
    }

    visBtn(cond) {
        if (cond === true) {
            clearTimeout(this.timer)
            this.setState({
                classBtn: 'fadeIn'
            })
        } else if (cond === false) {
            this.setState({
                classBtn: 'fadeOut'
            })
        }

    }

    timeOut() {
        this.timer = setTimeout(this.visBtn.bind(this, false), 2000)
    }

    inputHandle(e) {
        let rows = this.state.rows;
        rows[this.state.currentRow][this.state.currentCol].text = e.target.value
        this.setState({
            rows: rows
        })
    }

    handleBlur(e) {
        console.log(e.target.value)
        //this.setState({
        //    focus: false
        //})
    }
    
    render() {
        let that = this
        return <div className="wrapperTable">
            <table className="table" onMouseEnter={that.visBtn.bind(that, true)} onMouseLeave={that.timeOut.bind(that)}>
                <tbody>
                {this.state.rows.map(function (item, index) {
                    return <tr key={index}>{item.map(function (cell, cIndex) {
                        return <td className="table__tCell" key={cIndex}>
                            <input className="tCell__text"
                                   onBlur={that.handleBlur.bind(that)} type="text"
                                   onMouseEnter={that.hoverHandle.bind(that)} onInput={that.inputHandle.bind(that)}
                                   value={cell.text}/></td>
                    })}</tr>
                })}
                </tbody>
            </table>
            <Btn mouseLeave={this.timeOut.bind(that)} mouseEnter={this.visBtn.bind(that, true)}
                 changeClassBtn={this.state.classBtn} styleTop={this.state.top} styleLeft={this.state.left}
                 clickDelCol={this.delRowCol.bind(this, false)} clickDelRow={this.delRowCol.bind(this, true)}
                 clickAddRow={this.addColRow.bind(this, false)} clickAddCol={this.addColRow.bind(this, true)}/>
        </div>
    }
}

