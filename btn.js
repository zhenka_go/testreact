'use strict'
import React from 'react'

export default class Btn extends React.Component {

    propTypes () {
        return {
            clickAddRow: React.PropTypes.func.isRequired,
            clickAddCol: React.PropTypes.func.isRequired,
            clickDelRow: React.PropTypes.func.isRequired,
            clickDelCol: React.PropTypes.func.isRequired,
            styleTop: React.PropTypes.number,
            styleLeft: React.PropTypes.number,
            changeClassBtn: React.PropTypes.string,
            mouseEnterBtn: React.PropTypes.func.isRequired,
            mouseLeaveBtn: React.PropTypes.func.isRequired
        }
    }

    render () {
        return <div>
            <div onMouseLeave={this.props.mouseLeaveBtn} onMouseEnter={this.props.mouseEnterBtn} className="table__btn table__btn_AddRow" onClick={this.props.clickAddRow}><i className="fa fa-plus"></i></div>
            <div onMouseLeave={this.props.mouseLeaveBtn} onMouseEnter={this.props.mouseEnterBtn} className="table__btn table__btn_AddCell" onClick={this.props.clickAddCol}><i className="fa fa-plus"></i></div>
            <div onMouseLeave={this.props.mouseLeaveBtn} onMouseEnter={this.props.mouseEnterBtn} className={this.props.changeClassBtn + " " + "table__btn table__btn_DelCell animated"}  onClick={this.props.clickDelCol} style={{left: this.props.styleLeft + 'px'}}><i className="fa fa-minus"></i></div>
            <div onMouseLeave={this.props.mouseLeaveBtn} onMouseEnter={this.props.mouseEnterBtn} className={this.props.changeClassBtn + " " + "table__btn table__btn_DelRow  animated"}  onClick={this.props.clickDelRow} style={{top: this.props.styleTop + 'px'}} ><i className="fa fa-minus"></i></div></div>
    }
}

